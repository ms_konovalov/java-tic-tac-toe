package ms.konovalov.ttt;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Iterator;
import java.util.Optional;

import static ms.konovalov.ttt.Game3x3.MAX_PLAYERS;
import static org.easymock.EasyMock.mock;
import static org.junit.Assert.*;

@RunWith(DataProviderRunner.class)
public class Game3x3Test {

    @Test
    public void shouldSuccessfullyCreateGame() {
        Game g = new Game3x3();
        assertNotNull(g);
    }

    @Test
    public void shouldSuccessfullyAddFirstAndSecondPlayer() {
        Game g = new Game3x3();
        addAllPlayers(g);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldFailAddSamePlayers() {
        Game g = new Game3x3();
        Player player = mock(Player.class);
        g.join(player);
        g.join(player);
    }

    private void addAllPlayers(Game g) {
        for (int i = 0; i < MAX_PLAYERS; i++) {
            addPlayer("player" + i, g);
        }
        assertEquals(MAX_PLAYERS, g.getPlayers().size());
    }

    private Player addPlayer(String name, Game g) {
        Player player = new Player();
        g.join(player);
        assertTrue(g.getPlayers().contains(player));
        return player;
    }

    @Test(expected = IllegalStateException.class)
    public void shouldFailAddPlayersMoreThanAllowed() {
        Game g = new Game3x3();
        addAllPlayers(g);
        addPlayer("player", g);
    }

    @Test(expected = IllegalMoveException.class)
    public void shouldFailAddAMoveWithoutAnyPlayer() throws IllegalMoveException {
        Game g = new Game3x3();
        g.makeMove(new Move(mock(Player.class), 0, 0));
    }

    @Test(expected = IllegalMoveException.class)
    public void shouldFailAddAMoveWithWrongPlayer() throws IllegalMoveException {
        Game g = new Game3x3();
        addPlayer("player", g);
        g.makeMove(new Move(mock(Player.class), 0, 0));
    }

    @Test(expected = IllegalMoveException.class)
    public void shouldFailAddAMoveWithWrongPlayer2() throws IllegalMoveException {
        Game g = new Game3x3();
        addAllPlayers(g);
        g.makeMove(new Move(mock(Player.class), 0, 0));
    }

    @Test
    public void shouldSuccessfullyMakeARightMove() throws IllegalMoveException {
        Game g = new Game3x3();
        addAllPlayers(g);
        Player player = g.getPlayers().iterator().next();
        g.makeMove(new Move(player, 0, 0));
        assertFalse(g.getMoves().isEmpty());
        assertEquals(g.getMoves().size(), 1);
    }

    @Test
    public void shouldSuccessfullyMakeSeveralRightMoves() throws IllegalMoveException {
        Game g = new Game3x3();
        addAllPlayers(g);
        Iterator<Player> iterator = g.getPlayers().iterator();
        Player player1 = iterator.next();
        Player player2 = iterator.next();
        g.makeMove(new Move(player1, 0, 0));
        g.makeMove(new Move(player2, 1, 1));
        g.makeMove(new Move(player1, 2, 2));
        assertFalse(g.getMoves().isEmpty());
        assertEquals(g.getMoves().size(), 3);
    }

    @Test(expected = IllegalMoveException.class)
    public void shouldFailMakeDuplicateMoves() throws IllegalMoveException {
        Game g = new Game3x3();
        addAllPlayers(g);
        Iterator<Player> iterator = g.getPlayers().iterator();
        Player player1 = iterator.next();
        Player player2 = iterator.next();
        g.makeMove(new Move(player1, 0, 0));
        g.makeMove(new Move(player2, 0, 0));
    }

    @DataProvider
    public static Object[][] wrongMoves() {
        return new Object[][]{
                {0, 3}, {1, 9}, {2, 100}, {2, -1},
                {3, 0}, {100, 5}, {-5, 1},
                {-99, -99}, {10, 10}, {3, 3}
        };
    }

    @UseDataProvider("wrongMoves")
    @Test(expected = IllegalMoveException.class)
    public void shouldFailMakeWrongMoves(int x, int y) throws IllegalMoveException {
        Game g = new Game3x3();
        addAllPlayers(g);
        Player player1 = g.getPlayers().iterator().next();
        g.makeMove(new Move(player1, x, y));
    }

    @Test(expected = IllegalMoveException.class)
    public void shouldFailMakeMovesWrongOrder1() throws IllegalMoveException {
        Game g = new Game3x3();
        addAllPlayers(g);
        Player player1 = g.getPlayers().iterator().next();
        g.makeMove(new Move(player1, 0, 0));
        g.makeMove(new Move(player1, 1, 1));
    }

    @Test(expected = IllegalMoveException.class)
    public void shouldFailMakeMovesWrongOrder2() throws IllegalMoveException {
        Game g = new Game3x3();
        addAllPlayers(g);
        Iterator<Player> iterator = g.getPlayers().iterator();
        Player player1 = iterator.next();
        Player player2 = iterator.next();
        g.makeMove(new Move(player1, 0, 0));
        g.makeMove(new Move(player2, 1, 1));
        g.makeMove(new Move(player2, 1, 0));
    }

    @Test
    public void shouldSuccessfullyMakeOneMoveAfterJoiningOnePlayer() throws IllegalMoveException {
        Game g = new Game3x3();
        Player player1 = addPlayer("player1", g);
        g.makeMove(new Move(player1, 0, 0));
    }

    @Test(expected = IllegalMoveException.class)
    public void shouldFailMakeSecondMoveAfterJoiningOnePlayer() throws IllegalMoveException {
        Game g = new Game3x3();
        Player player1 = addPlayer("player1", g);
        g.makeMove(new Move(player1, 0, 0));
        g.makeMove(new Move(player1, 1, 0));
    }

    @Test
    public void shouldSuccessfullyMakeOneMoveAfterJoiningOnePlayerThenJoinSecondPlayerAndMakeMoreMoves() throws IllegalMoveException {
        Game g = new Game3x3();
        Player player1 = addPlayer("player1", g);
        g.makeMove(new Move(player1, 0, 0));
        Player player2 = addPlayer("player2", g);
        g.makeMove(new Move(player2, 1, 0));
        g.makeMove(new Move(player1, 0, 1));
        g.makeMove(new Move(player2, 0, 2));
    }

    @Test
    public void shouldSuccessfullyFinishGameWithDraw() throws IllegalMoveException {
        Game g = new Game3x3();
        Player player1 = addPlayer("player1", g);
        Player player2 = addPlayer("player2", g);
        makeDraw(g, player1, player2);
    }

    @Test(expected = IllegalMoveException.class)
    public void shouldFailToMakeMoveAfterDraw() throws IllegalMoveException {
        Game g = new Game3x3();
        Player player1 = addPlayer("player1", g);
        Player player2 = addPlayer("player2", g);
        makeDraw(g, player1, player2);
        g.makeMove(new Move(player2, 0, 1));
    }

    private void makeDraw(Game g, Player player1, Player player2) throws IllegalMoveException {
        Move[] moves = new Move[]{
                new Move(player1, 0, 0),
                new Move(player2, 2, 0),
                new Move(player1, 1, 0),
                new Move(player2, 0, 1),
                new Move(player1, 0, 2),
                new Move(player2, 1, 1),
                new Move(player1, 2, 1),
                new Move(player2, 1, 2),
                new Move(player1, 2, 2)
        };
        Optional<GameResult> result = Optional.empty();
        for (Move move : moves) {
            result = g.makeMove(move);
        }
        assertTrue(result.isPresent() && result.get() instanceof GameResult.Draw);
    }

    @Test
    public void shouldSuccessfullyFinishGameWithVictory1() throws IllegalMoveException {
        Game g = new Game3x3();
        Player player1 = addPlayer("player1", g);
        Player player2 = addPlayer("player2", g);
        Move[] moves = new Move[]{
                new Move(player1, 0, 0),
                new Move(player2, 0, 1),
                new Move(player1, 1, 0),
                new Move(player2, 1, 1),
                new Move(player1, 2, 0)
        };
        Optional<GameResult> result = Optional.empty();
        for (Move move : moves) {
            result = g.makeMove(move);
        }
        assertTrue(result.isPresent() && result.get() instanceof GameResult.Victory);
    }

}