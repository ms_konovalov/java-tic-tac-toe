package ms.konovalov.ttt;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
class Move {

    private final Player player;
    private final int x;
    private final int y;

}
