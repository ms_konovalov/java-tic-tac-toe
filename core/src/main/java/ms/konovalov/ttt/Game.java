package ms.konovalov.ttt;

import java.util.Collection;
import java.util.Optional;

public interface Game {

    public void join(Player player);

    public Collection<Player> getPlayers();

    public Collection<Move> getMoves();

    Optional<GameResult> makeMove(Move move) throws IllegalMoveException;

}
