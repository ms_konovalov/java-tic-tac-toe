package ms.konovalov.ttt;

import lombok.Getter;
import lombok.Setter;

public class Player {

    @Getter
    @Setter
    private int sign;

}
