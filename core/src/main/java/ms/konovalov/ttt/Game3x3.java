package ms.konovalov.ttt;

import lombok.Getter;

import java.util.*;
import java.util.stream.Stream;

//Not thread safe
public class Game3x3 implements Game {

    private static final int BOARD_SIZE = 3;
    static final int MAX_PLAYERS = 2;

    private static final int[] PLAYERS_SIGNS = new int[]{1, -1};
    private List<Player> players = new ArrayList<>(MAX_PLAYERS);

    private Deque<Move> moves = new ArrayDeque<>(BOARD_SIZE * BOARD_SIZE);

    private int[] score = new int[BOARD_SIZE * 2 + 2];

    @Getter
    private Optional<GameResult> result = Optional.empty();

    @Override
    public void join(Player player) {
        if (players.size() >= MAX_PLAYERS) {
            throw new IllegalStateException("Failed to add more players than allowed");
        }
        if (players.contains(player)) {
            throw new IllegalStateException("Failed to add same players");
        }
        //TODO make thread-safe
        player.setSign(PLAYERS_SIGNS[players.size()]);
        players.add(player);
    }

    @Override
    public Optional<GameResult> makeMove(Move move) throws IllegalMoveException {
        checkPlayer(move);
        checkCellHasCorrectCoordinates(move);
        checkCellIsFree(move);
        moves.add(move);
        result = calculateResult(move);
        return getResult();
    }

    private Optional<GameResult> calculateResult(Move move) {
        if (moves.size() >= BOARD_SIZE * BOARD_SIZE) {
            return Optional.of(new GameResult.Draw());
        }
        score[move.getX()] += move.getPlayer().getSign(); // where point is either +1 or -1
        score[BOARD_SIZE + move.getY()] += move.getPlayer().getSign();
        if (move.getX() == move.getY()) score[2 * BOARD_SIZE] += move.getPlayer().getSign();
        if (BOARD_SIZE - 1 - move.getY() == move.getX()) score[2 * BOARD_SIZE + 1] += move.getPlayer().getSign();
        if (Arrays.stream(score).anyMatch(x -> x == BOARD_SIZE)) {
            return Optional.of(new GameResult.Victory());
        }
        return Optional.empty();
    }

    private void checkPlayer(Move move) throws IllegalMoveException {
        if (!players.contains(move.getPlayer())) {
            throw new IllegalMoveException("This player has not joined the game");
        }
        Player nextPlayer;
        try {
            nextPlayer = getNextPlayer();
        } catch (IllegalStateException e) {
            throw new IllegalMoveException(e.getMessage());
        }
        if (nextPlayer != null && nextPlayer != move.getPlayer()) {
            throw new IllegalMoveException("Current turn is for another player");
        }
    }

    //TODO make thread-safe
    public Player getNextPlayer() {
        if (moves.isEmpty()) {
            return null;
        }
        Player lastPlayer = moves.getLast().getPlayer();
        Iterator<Player> iterator = players.iterator();
        while (iterator.hasNext()) {
            if (iterator.next() == lastPlayer) {
                if (iterator.hasNext()) {
                    return iterator.next();
                }
                if (players.size() < MAX_PLAYERS) {
                    throw new IllegalStateException("Not all players are joined the game");
                } else {
                    return players.iterator().next();
                }
            }
        }
        //should never be reached
        throw new IllegalStateException("No available players");
    }

    private void checkCellHasCorrectCoordinates(Move move) throws IllegalMoveException {
        if (move.getX() >= BOARD_SIZE || move.getX() < 0 || move.getY() >= BOARD_SIZE || move.getY() < 0) {
            throw new IllegalMoveException("Wrong cell coordinates");
        }
    }

    private void checkCellIsFree(Move move) throws IllegalMoveException {
        if (moves.stream().anyMatch(m -> m.getX() == move.getX() && m.getY() == move.getY())) {
            throw new IllegalMoveException("This cell is already occupied");
        }
    }

    public Collection<Player> getPlayers() {
        return Collections.unmodifiableList(players);
    }

    public Collection<Move> getMoves() {
        return Collections.unmodifiableCollection(moves);
    }
}
