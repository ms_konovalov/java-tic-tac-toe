package ms.konovalov.ttt;

public class IllegalMoveException extends Exception {

    public IllegalMoveException(String s) {
        super(s);
    }
}
