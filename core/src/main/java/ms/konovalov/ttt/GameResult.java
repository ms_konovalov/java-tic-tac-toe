package ms.konovalov.ttt;

public interface GameResult {

    class Draw implements GameResult {
    }

    class Victory implements GameResult {
    }
}
